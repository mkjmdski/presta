ARG PHP_VERSION
FROM registry.gitlab.com/mkjmdski/presta/${PHP_VERSION}/prebuild:latest
COPY --chown=www-data:www-data backend /app/backend
