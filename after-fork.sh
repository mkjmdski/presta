#!/bin/bash
function hardcode_php_version {
    export PHP_VERSION=$(git rev-parse --abbrev-ref HEAD)
    envsubst < .gitlab-ci.after-fork.yml > .gitlab-ci.yml
    rm .gitlab-ci.after-fork.yml
    envsubst < docker-compose.template.yaml > docker-compose.yaml
    rm docker-compose.template.yaml
}

function remove_garbage {
    rm after-fork.sh
    rm -rf prebuild
}


hardcode_php_version
remove_garbage
