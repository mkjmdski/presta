# Presta Shop on Docker

Inspired by [official](https://github.com/PrestaShop/docker) prestashop docker image, but targets specifically against constant developer code base (meaning docker build process isn't generic for all php versions)

## 1. How to start project

1. Fork this repository
2. Change 'presta' in local.env and docker-compose.yaml for the name of your project

Now you have two options

### 1.1 Start new shop

1. Add fresh presta files (after second unzipping) to the `backend` directory
2. Run `pwgen -C -n -y -s 40 1` to generate new presta_secret and set it in the `conf/parameters.yml`
3. Start backend by running `docker-compose up --force-recreate --build -d` and afterwards install presta by running `docker-compose exec app sh /start.sh INSTALL`

### 1.2 Continue with existing shop

1. Add your existing presta project to the backend directory and continue development

## 2. Managing database

You can use phpmyadmin on port 8080 to control your DB. If you want to get dump from CLI execute `docker-compose exec app sh /start.sh GET_DUMP [name]`, it will be saved as the name you passed or as `[current_date].sql` inside `dump` directory. If you want to load dump execute `docker-compose exec app sh /start.sh LOAD_DUMP [name]` to load the dump of `[name]` from `dump` directory or `init.sql` if no name has been passed.

## 3. Runing container

Use env variables to control the application. If you see blank page check if UID of www-data inside container matchers UID of the user which runs as owner of backend directory volume on host machine

### 3.1 Env variables

All env variables are explained inside local.env file, use them to configure your containers.